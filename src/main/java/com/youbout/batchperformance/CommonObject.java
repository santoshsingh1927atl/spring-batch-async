package com.youbout.batchperformance;

import lombok.Data;
import lombok.Generated;

@Data
@Generated
public class CommonObject {
	private String num;
    private String completeRecord;
    private String type;

}
