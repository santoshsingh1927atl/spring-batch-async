package com.youbout.batchperformance.genratefiles;

import java.io.FileWriter;

public class Data {

	public static void main(String[] args) {

		long mainTime = System.currentTimeMillis();
		try {

			Thread t1 = new Thread(new Runnable() {
				public void run() {
					System.out.println("Input File Started.");
					long time = System.currentTimeMillis();
					try {
						FileWriter myWriter = new FileWriter("input.txt");
						
						for(long i = 1 ; i < 50_000_000;i++) {
							myWriter.write(getKey((int)(Math.random() * 100000000)+"", 9)+ "," + (int) (Math.random() * 10000) +"," + ("test1,test" + i));
							myWriter.write(System.lineSeparator());
						}
						myWriter.close();
						System.out.println("Input File Time Taken - " + (System.currentTimeMillis() - time));
						
					} catch (Exception e) {
						e.printStackTrace();
					}finally {
						synchronized (this) {
							System.out.println("NotifyAll Threads....");
							this.notifyAll();
						}
					}
				}

			}, "InputFile");

			
			t1.start();
			synchronized (t1) {
				System.out.println("Waiting for thread to complete....");
				t1.wait();
			}
			
			System.out.println("File Wrting done in and sorting started - " + (System.currentTimeMillis() - mainTime));
			mainTime = System.currentTimeMillis();
			Process p = Runtime.getRuntime().exec("sort -n input.txt > output.txt" );
			System.out.println("Sorting Done in - " + (System.currentTimeMillis() - mainTime));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
	
	public static String getKey(String in, int count) throws InterruptedException {
		int missingCount = count - in.length();
		StringBuffer res = new StringBuffer();
		res.append("1");
		for(int i = 0; i < missingCount; i++) {
			res.append("0");
		}
		//Thread.sleep(0, 1);
		return res.append(in).toString();
	}
}
