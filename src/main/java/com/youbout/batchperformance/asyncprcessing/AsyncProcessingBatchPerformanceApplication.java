package com.youbout.batchperformance.asyncprcessing;

import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.integration.async.AsyncItemProcessor;
import org.springframework.batch.integration.async.AsyncItemWriter;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.youbout.batchperformance.CommonObject;

@SpringBootApplication
@EnableBatchProcessing
public class AsyncProcessingBatchPerformanceApplication {
	public static final String ROOT_PATH = "C:\\java\\tempfiles\\";
    public static void main(String[] args) {
        SpringApplication.run(AsyncProcessingBatchPerformanceApplication.class, args);
        System.out.println("AppStarted......");
    }

    @Autowired
    StepBuilderFactory stepBuilderFactory;
    
    @Bean
    public Job asyncJob(JobBuilderFactory jobBuilderFactory) {
        return jobBuilderFactory
                .get("ASYNCHRONOUS_PROCESSING_JOB")
                .incrementer(new RunIdIncrementer())
                .start(asyncManagerStep())
                .build();
    }

    @Bean
    public Step asyncManagerStep() {
        return stepBuilderFactory
                .get("ASYNCHRONOUS_PROCESSING___READ--->PROCESS----->WRITE")
                .<CommonObject, Future<CommonObject>>chunk(200000)
                .reader(asyncReader())
                .processor(asyncProcessor())
                .writer(asyncWriter())
                .taskExecutor(taskExecutor())
                .build();
    }
    
    @Bean
    public AsyncItemProcessor<CommonObject, CommonObject> asyncProcessor() {
        AsyncItemProcessor<CommonObject, CommonObject> asyncItemProcessor = new AsyncItemProcessor<>();
        asyncItemProcessor.setDelegate(itemProcessor());
        asyncItemProcessor.setTaskExecutor(taskExecutor());

        return asyncItemProcessor;
    }

    @Bean
    public AsyncItemWriter<CommonObject> asyncWriter() {
        AsyncItemWriter<CommonObject> asyncItemWriter = new AsyncItemWriter<>();
        asyncItemWriter.setDelegate(itemWriter());
        return asyncItemWriter;
    }

    @Bean
    public TaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(100000);
        executor.setMaxPoolSize(200000);
        executor.setQueueCapacity(300000);
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        executor.setThreadNamePrefix("MultiThreaded-");
        return executor;
    }

    @Bean
    public ItemProcessor<CommonObject, CommonObject> itemProcessor() {
        return (commonObject) -> {
        	//System.out.println("Processs---" + commonObject.getNum());
            Thread.sleep(2000);
            //commonObject.setCompleteRecord(commonObject.getCompleteRecord().toUpperCase());
            return commonObject;
        };
    }

    
    @Bean
    public ItemReader<CommonObject> asyncReader() {

    	return new FlatFileItemReaderBuilder<CommonObject>().name("CommonObjectItemReader")
                .resource(new FileSystemResource(ROOT_PATH + "input.txt"))
                .delimited()
                .names(new String[] {"num", "completeRecord"})
                .fieldSetMapper(new BeanWrapperFieldSetMapper<CommonObject>() {{
                 setTargetType(CommonObject.class);
                 }})
                .build();
    }

    @Bean
    public FlatFileItemWriter<CommonObject> itemWriter() {

        return new FlatFileItemWriterBuilder<CommonObject>()
                .name("Writer")
                .append(false)
                .resource(new FileSystemResource(ROOT_PATH + "output.txt"))
                .lineAggregator(new DelimitedLineAggregator<CommonObject>() {
                    {
                        setDelimiter(",");
                        setFieldExtractor(new BeanWrapperFieldExtractor<CommonObject>() {
                            {
                                setNames(new String[]{"num", "completeRecord"});
                            }
                        });
                    }
                })
                .build();
    }
}
